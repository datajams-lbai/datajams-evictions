/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const Court = require("./lib/Court.class");
const sqDAO = require("./lib/sqlDAO");
const fs = require("fs");

if (process.argv.length < 5) throw "Must pass in precinct, place, file name.";

let court = new Court({
    precinct: process.argv[2],
    place: process.argv[3]
});


let opts = require("./creds")["SQLITE3"];
opts.connectCallback = sqDB => {
    let exOpts = {
	database: sqDB,
	extractStream: fs.createReadStream(process.argv[4])
    };
    court.parseExtract(exOpts)
	.then(vals => {
	    vals.forEach(val => {
		console.log("Case", val, "stored");
	    });
	})
	.catch(err => {
	    console.error("ERROR", err);
	})
    ;
};
sqDAO.connect(opts);
