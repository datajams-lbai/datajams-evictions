/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const https = require("https");
const httpOpts = require("./httpOptions");
const util = require("util");
const hp = require("htmlparser2");
const DomHandler = require("domhandler").DomHandler;
const du = require("domutils");
const Case = require("./Case.class");

module.exports = class Docket {
    constructor(argv) {
	if (!argv.court) {
	    throw new Error("Must have a parent court.");
	}
	this.court = argv.court;

	this.docketType = argv.docketType || "Eviction Docket";	    
	
	if (!argv.URL) {
	    if (!argv.date || !argv.time)
		throw new Error("Must include a URL.");
	    this.URL = new URL("https://jpwebsite.harriscountytx.gov/WebDockets/GetDocketDetail");
	    this.URL.search = "date="+argv.date+"&time="+argv.time;
	} else {
	    this.URL = argv.URL;
	}
	this.date = this.URL.searchParams.get("date");
	let oTime = this.URL.searchParams.get("time");
	this.time = oTime.substr(0, 2) + ":" + oTime.substr(2, 2) + ":" + oTime.substr(4);
    };

    toString() {
	return JSON.stringify({
	    court: this.court.toString(),
	    docketType: this.docketType,
	    url: this.url.toString(),
	    cases: this.cases
	});
    };

    getCases(caseCallback) {
	return new Promise((resFN, rejFN) => {
	    let opts = new httpOpts();

	    // With the data all set up, now connect and parse:
	    let req = https.request(this.URL, opts, (res) => {
		let handler = new DomHandler((err, dom) => {
		    if (err) {
			rejFN({ url: this.URL, err: err });
		    } else {
			// Find the containing node:
			let events = du.filter((node) => {
			    return ("div" === node.name && "contentWithNavBar" === node.attribs.class);
			}, dom);
			// Then get the div.caseDetails nodes within them:
			let cases = du.filter((node) => {
			    return ("div" === node.name && "caseDetails" === node.attribs.class);
			}, events);
			// From those, get the span.emphasis nodes:
			let urlSpans = du.filter((node) => {
			    if ("span" === node.name && "emphasis" === node.attribs.class) {
				// Check the siblings to make sure they're the event type we want:
				let sibList = du.filter((sib) => {
				    return ("text" === sib.type && sib.data.trim() === this.docketType);
				}, du.getSiblings(node));
				return (sibList.length > 0);
			    } else {
				return false;
			    }
			}, cases);		
			
			// In those, grab the "a" nodes:
			let urlNodes = du.filter((node) => {
			    return ("a" === node.name);
			}, urlSpans);

			// For each of these, grab the hrefs:
			this.cases = urlNodes.map((node) => {
			    return new Case(this, new URL(node.attribs.href, this.URL).toString());
			});

			Promise.all(
			    this.cases.map((caseObj) => {
				return caseObj.loadCaseFromURL()
				    .then(caseO => {
					caseCallback && "function" === typeof caseCallback && caseCallback(caseO);
				    })
				    .catch(err => { rejFN(err); })
				;
			    })
			)
			    .then(val => {
				resFN({
				    docket: this,
				    cases: val.map(v => { return v; })
				});
			    })
			    .catch(err => { rejFN(err); })
			;
		    };
		}, {
		    normalizeWhitespace: true
		});
		let parser = new hp.Parser(handler);

		// What to do with incoming data:
		res.on("data", (d) => {
		    parser.parseChunk(d);
		}).on("end", () => {
		    parser.done();
		});
		
	    });

	    req.on('error', (e) => {
		rejFN({ msg: "Load Docket HTTP", err: e });
	    });

	    // Ok GO:
	    req.end();
	});
    };
};
