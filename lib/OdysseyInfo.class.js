/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const https = require("https");
const httpOpts = require("./httpOptions");
const HTTPConversation = require("./HTTPConversation.class");
const util = require("util");
const hp = require("htmlparser2");
const DomHandler = require("domhandler").DomHandler;
const du = require("domutils");

function getCaseIDParser(oiObj, callback, errFN) {
    // <a class="caseLink" data-caseid="jBTdKQrus6pMdAW2SPPMBA2" data-caseid-num="6402180">205100178940</a>
    let handler = new DomHandler((err, dom) => {
	if (err) {
	    errFN && "function" === typeof errFN && errFN({ msg: "caseIDParser", err: err });
	    return;
	} else {
	    // Find the containing node:
	    let idLinks = du.filter(node => {
		return ("a" === node.name && "caseLink" === node.attribs.class);
	    }, dom);
	    
	    if (1 !== idLinks.length) {
		errFN && "function" === typeof errFN && errFN({ msg: "caseIDParser: unable to find caseID", html: du.getOuterHTML(dom) });
		return;
	    }

	    try {
		oiObj.caseID = idLinks[0].attribs["data-caseid"];		
		oiObj.caseURL = idLinks[0].attribs["data-url"];
		if (!oiObj.caseURL && oiObj.debug) {
		    console.log("Can't find data-URL", idLinks[0], oiObj);
		}
		// Now use the callback to process the hrefs:
		(callback && "function" === typeof callback) && callback();
	    } catch (e) {
		console.error(e, idLinks);
	    }
	};
    }, {
	normalizeWhitespace: true
    });
    
    return new hp.Parser(handler);	
};

function getCaseInfoParser(oiObj, callback, errFN) {
    // <a class="caseLink" data-caseid="jBTdKQrus6pMdAW2SPPMBA2" data-caseid-num="6402180">205100178940</a>
    let handler = new DomHandler((err, dom) => {
	if (err) {
	    errFN && "function" === typeof errFN && errFN({ msg: "caseInfoParser", err: err });
	    return;
	} else {
	    oiObj.debug && console.log("Finding party info");
	    
	    // Find the containing node:
	    let partyInfo = du.filter(node => {
		return ("div" === node.name && "divPartyInformation_body" === node.attribs.id);
	    }, dom);
	    
	    if (1 !== partyInfo.length) {
		errFN && "function" === typeof errFN && errFN({ msg: "caseIDParser: unable to find party info", oi: oiObj });
		return;
	    }

	    // Each party is in a <div class="col-md-8"/>
	    let partyDivs = du.filter(node => {
		return ("div" === node.name && "col-md-8" === node.attribs.class);
	    }, partyInfo);

	    // Each of those has a <p><span>class</span>name</p> and <p>Address</p>:
	    oiObj.parties = [];
	    partyDivs.forEach(partyDiv => {
		let party = {};
		let pDivs = du.filter(node => {
		    return ("p" === node.name);
		}, partyDiv);

		// pDivs[0]: <p><span>class</span>name</p>
		du.filter(node => {
		    return ("span" === node.name);
		}, pDivs[0]).forEach(span => {
		    // SHOULD only be ONE of these:
		    party.role = du.filter(node => {
			return ("text" === node.type);
		    }, span).map(val => {
			return val.data;
		    }).join(" ").trim();
		    // Get rid of the span for the next manipulation:
		    du.removeElement(span);
		});
		party.name = du.filter(node => {
		    return ("text" === node.type);
		}, pDivs[0]).map(val => {
		    return val.data;
		}).join(" ").trim();

		// pDivs[1]: Address
		if (pDivs[1]) {
		    try {
			party.address = du.filter(node => {
			    return ("text" === node.type);
			}, pDivs[1]).map(val => {
			    return val.data;
			});
		    } catch (e) {
			console.error("OI 111", pDivs[1], e);
		    }
		}
		
		oiObj.parties.push(party);
	    });

	    // Now do the callback:
	    callback && "function" === typeof callback && callback(oiObj.parties);
	};
    }, {
	normalizeWhitespace: true
    });
    
    return new hp.Parser(handler);	
};

module.exports = class OdysseyInfo {
    constructor(caseNumber, argv) {
	if (! caseNumber) {
	    throw new Error("Must define a case number!");
	}
	let opts = argv || {};
	this.caseNumber = caseNumber;
	this.debug = opts.debug;
	this.verbose = opts.verbose;
    };
    
    findCasePromise() {
	return new Promise((resFN, rejFN) => {
	    let host = "odysseyportal.harriscountytx.gov";
	    let hc = new HTTPConversation({ rejectFN: rejFN });

	    this.verbose && console.log("Getting front page", this);
	    hc.getURL({ reqHost: host, reqPath: "/OdysseyPortalJP", callback: () => {
		this.verbose && console.log("Got front page.");
		hc.getURL({ reqHost: host, reqPath: "/OdysseyPortalJP/Home/Dashboard/29", callback: () => {
		    this.verbose && console.log("Got Dashboard page.");
		    hc.postURLData({
			reqHost: host,
			reqPath: "/OdysseyPortalJP/SmartSearch/SmartSearch/SmartSearch",
			data: [
			    [ "Settings.CaptchaEnabled", "False" ],
			    [ "Settings.CaptchaDisabledForAuthenticated", "False" ],
			    [ "caseCriteria.SearchCriteria", this.caseNumber ],
			    [ "caseCriteria.JudicialOfficerSearchBy", "" ],
			    [ "caseCriteria.NameMiddle", "" ],
			    [ "caseCriteria.NameSuffix", "" ],
			    [ "caseCriteria.AdvancedSearchOptionsOpen", "false" ],
			    [ "caseCriteria.CourtLocation_input", "Harris County JPs Odyssey Portal" ],
			    [ "caseCriteria.CourtLocation", "Harris County JPs Odyssey Portal" ],
			    [ "caseCriteria.SearchBy_input", "Smart Search" ],
			    [ "caseCriteria.SearchBy", "SmartSearch" ],
 			    [ "caseCriteria.SearchCases", "true" ],
 			    [ "caseCriteria.SearchCases", "false" ],
			    [ "caseCriteria.SearchJudgments", "true" ],
			    [ "caseCriteria.SearchJudgments", "false" ],
			    [ "caseCriteria.SearchByPartyName", "true" ],
			    [ "caseCriteria.SearchByNickName", "false" ],
			    [ "caseCriteria.SearchByBusinessName", "false" ],
			    [ "caseCriteria.UseSoundex", "false" ],
			    [ "caseCriteria.Gender_input", "" ],
			    [ "caseCriteria.Gender", "" ],
			    [ "caseCriteria.Race_input", "" ],
			    [ "caseCriteria.Race", "" ],
			    [ "caseCriteria.FBINumber", "" ],
			    [ "caseCriteria.SONumber", "" ],
			    [ "caseCriteria.BookingNumber", "" ],
			    [ "caseCriteria.CaseType_input", "" ],
			    [ "caseCriteria.CaseType", "" ],
			    [ "caseCriteria.CaseStatus_input", "" ],
			    [ "caseCriteria.CaseStatus", "" ],
			    [ "caseCriteria.FileDateStart", "" ],
			    [ "caseCriteria.FileDateEnd", "" ],
			    [ "caseCriteria.JudicialOfficer_input", "" ],
			    [ "caseCriteria.JudicialOfficer", "" ],
			    [ "caseCriteria.JudgmentType_input", "" ],
			    [ "caseCriteria.JudgmentType", "" ],
			    [ "caseCriteria.JudgmentDateFrom", "" ],
			    [ "caseCriteria.JudgmentDateTo", "" ]	    
			],
			callback: () => {
			    this.verbose && console.log("Got POST data, calling workspace.");
			    hc.getURL({ reqHost: host, reqPath: "/OdysseyPortalJP/Home/WorkspaceMode?p=0", callback: () => {
				setTimeout(() => {
				    hc.getURL({
					reqHost: host,
					reqPath: "/OdysseyPortalJP/SmartSearch/SmartSearchResults?_="+(new Date()).valueOf(),
					parser: getCaseIDParser(this, () => {
					    this.debug && console.log("CaseID: ", this.caseID);
					    hc.getURL({
						reqHost: host,
						reqPath: "/OdysseyPortalJP/Case/CaseDetail?eid="+this.caseID+"&tabIndex=3&_="+(new Date()).valueOf(),
						// reqPath: this.caseURL + "&_="+(new Date()).valueOf(),
						parser: getCaseInfoParser(this, () => {
						    if (resFN && "function" === typeof resFN) {
							resFN(this);
						    } else {
							console.log(this);
						    }
						}, rejFN)
					    });
					}, rejFN)
				    });
				}, 1000);
			    }});
			}});
		}});
	    }});
	});
    };
};
