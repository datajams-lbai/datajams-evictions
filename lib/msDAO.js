/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const sql = require("mssql");

let DAO = class {
    constructor(opts) {
    };
};

module.exports = {
    connect(opts) {
	if (!opts)  throw "Must set credentials and callback";
	if (!opts.user) throw "Must set user in opts";
	if (!opts.pass) throw "Must set pass in opts";

	let config = {
	    user: opts.user,
	    password: opts.pass,
	    server: "harriscountyhousing.database.windows.net",
	    database: "DataJams_HarrisCountyHousing",
	    encrypt: true,
	    options: {
		enableArithAbort: true
	    }
	};

	sql.connect(config)
	    .then(() => {
		opts.connectCallback && "function" === typeof opts.connectCallback && opts.connectCallback(sql);
	    })
	    .catch(err => { console.log("Connect Error: ", err); })
	;
    },

    disconnect(database) {
	database.close().catch(err => { console.log("Close Error: ", err); });
    }
};
