/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const sq3 = require("sqlite3");

let DAO = class {

    constructor() {
	this.stmts = {};
    };

    finalize() {
	for (var s in this.stmts) {
	    this.stmts[s].finalize();
	}
    };
    
    reportErr(e) {
	console.error(typeof this.dao, e);
    };

    prepare(database, sql) {
	let stmt = database.prepare(sql);
	stmt.dao = this;
	return stmt;
    };
    
    insert(dObj) {
	this.dataObj = dObj;
	if (this.stmts.insert) {
	    this.stmts.insert.run(dObj, this.errInsert);
	} else {
	    this.reportErr({ error: "No INSERT statement?" });
	}
    };
    errInsert(e) {
	if (e) {
	    switch (e.errno) {
	    case 19:
		if (!this.ignoreInsertErrors) {
		    if (this.dao.stmts.update) {
			console.error("I would do an update here: ", this.dao.dataObj);
		    } else {
			// In general, ignore the conflict:
		    }
		}
		break;
	    default:
		this.dao.reportErr(e);
		break;
	    }
	}
    };

    update(dObj) {
	this.dataObj = dObj;
	if (this.stmts.update) {
	    this.stmts.update.run(dObj, this.errUpdate);
	} else {
	    this.reportErr({ error: "No UPDATE statement?" });
	}
    };
    errUpdate(e) {
	if (e) {
	    switch (e.errno) {
	    default:
		this.dao.reportErr(e);
		break;
	    }
	}
    };
};

module.exports = {
    DAO: DAO,

    connect(opts) {
	sq3.verbose();
	if (!opts || !opts.fileName) {
	    throw "Must include .fileName in opts!";
	}
	let database = new sq3.Database(opts.fileName, err => {
	    if (err) {
		throw { db: opts, err: err};
	    }
	});
	opts && "function" === typeof opts.connectCallback && opts.connectCallback(database);
    },

    disconnect(db) {
	db.close();
    },
    
    DocketCtlr: class extends DAO {
	constructor (database) {
	    super();	    
	    this.stmts.insert = this.prepare(
		database,
		"INSERT INTO docket (precinct, place, docket_dateTime, URL) VALUES ($pct, $plc, $dt, $url)"
	    );
	};
	
    },
    DocketCaseCtlr: class extends DAO {
	constructor(database) {
	    super();
	    this.stmts.insert = this.prepare(
		database,
		"INSERT INTO docketedCases (precinct, place, docket_dateTIme, casenumber, claim) VALUES ($pct, $plc, $dt, $cnum, $claim)"
	    );
	};
    },
    CaseCtlr: class extends DAO {
	constructor(database) {
	    super();
	    this.stmts.insert = this.prepare(
		database,
		"INSERT INTO cases (casenumber, case_URL, case_status, filed_Date) VALUES ($cnum, $curl, $cstat, $fd)"
	    );
	};
    },
    PartyCtlr: class extends DAO {
	constructor(database) {
	    super();
	    this.stmts.insert = this.prepare(
		database,
		"INSERT INTO party (casenumber, party_role, party_name, party_address) VALUES ($cnum, $role, $name, $addr)"
	    );
	};
    },
    EventCtlr: class extends DAO {
	constructor(database) {
	    super();
	    this.stmts.insert = this.prepare(
		database,
		"INSERT INTO events (casenumber, eventDescription, dateAdded) VALUES ($cnum, $desc, $date)"
	    );
	};
    }
};

