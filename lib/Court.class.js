/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const https = require("https");
const httpOpts = require("./httpOptions");
const util = require("util");
const hp = require("htmlparser2");
const DomHandler = require("domhandler").DomHandler;
const du = require("domutils");
const Docket = require("./Docket.class");
const Case = require("./Case.class");
const moment = require("dayjs");
moment.extend(require("dayjs/plugin/customParseFormat"));

// Create parser:
let getExtractInfo = node => {
    if (node && node.children && node.children[0] && node.children[0].data)
	return node.children[0].data.trim();
    else
	return undefined;
};
let extractAddParty = (caseObj, row, pfx, role) => {
    let party = {
	role: role
    };
    let addr = {
	line1: "",
	line2: "",
	city: "",
	state: "",
	zip: ""
    };

    du.filter(node => {
	if (node.name && node.name.startsWith(pfx)) {
	    
	    switch (node.name.substring(pfx.length)) {
	    case "name":
		party.name = getExtractInfo(node);
		break;
	    case "addressline1":
		addr.line1 = getExtractInfo(node);
		break;
	    case "addressline2":
		addr.line2 = getExtractInfo(node);
		break;
	    case "addresscity":
		addr.city = getExtractInfo(node);
		break;
	    case "addressstate":
		addr.state = getExtractInfo(node);
		break;
	    case "addresszip1":
		addr.zip = getExtractInfo(node);
		break;
	    }
	}
    }, row);
    
    party.address = addr.line1 + ", " + (addr.line2 ? addr.line2 + ", " : "") + addr.city + ", " + addr.state + " " + addr.zip;

    if (party.name)
	caseObj.parties.push(party);

};

let extractParserGenerator = (court, resFN, rejFN, database) => {
    let casePromises = [];
    return new hp.Parser(
	new DomHandler((err, dom) => {
	    if (err) {
		rejFN({ url: this.URL, err: err });
	    } else {
		// Find the result nodes:
		let rows = du.filter(node => {
		    return ("row" === node.name);
		}, dom);

		rows.forEach(row => {
		    let info = {
			parties: [],
			events: []
		    };
		    
		    du.filter(node => {
			switch (node.name) {
			case "casenumber":
			    info.caseNumber = getExtractInfo(node);
			    break;
			case "nexteventdate":
			    info.eventDate = getExtractInfo(node);
			    break;
			case "cai3dv":
			    info.eventTime = getExtractInfo(node);
			    break;
			case "nexteventdescription":
			    info.eventDescription = getExtractInfo(node);
			    break;
			case "fileddate":
			    info.filedDate = getExtractInfo(node);
			    break;
			case "natureofclaim":
			    info.natureOfClaim = getExtractInfo(node);
			    break;
			case "casestatus":
			    info.caseStatus = getExtractInfo(node);
			    break;
			case "claimamount":
			    info.claimAmount = getExtractInfo(node);
			    break;
			};
		    }, row);

		    if (info.eventDescription) {
			info.events.push({
			    description: info.eventDescription,
			    dateAdded: info.eventDate
			});
		    }

		    // This is not as thorough as loading from URL.
		    // Too bad. It's also fast.
		    // Should probably have an "updateParties" at some point.
		    extractAddParty(info, row, "plaintiff", "Plaintiff");
		    extractAddParty(info, row, "plaintiffatty", "Attorney");
		    extractAddParty(info, row, "defendant", "Defendant");
		    extractAddParty(info, row, "defendantatty", "Attorney");
		    extractAddParty(info, row, "secondplaintiff", "Plaintiff");
		    extractAddParty(info, row, "secondplaintiffatty", "Attorney");
		    extractAddParty(info, row, "seconddefendant", "Defendant");
		    extractAddParty(info, row, "seconddefendantatty", "Attorney");

		    let docket = null;
		    if (info.eventDate && info.eventTime) {
			let md = moment(info.eventTime, "hh:mm A");
			docket = new Docket({
			    court: court,
			    date: info.eventDate,
			    time: md.format("HHmm00")
			});
		    }

		    // Don't need a deep clone here, "info" goes away:
		    let caseObj = Object.assign(
			new Case(docket, null, info.caseNumber),
			info
		    );
		    try {
			caseObj.storeSQL(database);
			casePromises.push(info.caseNumber);
		    } catch (e) {			
			console.error("Can't store case?", caseObj, e);
		    }
		});

		resFN(casePromises);		
	    };
	    
	}, {
	    normalizeWhitespace: true
	})
    );
};

module.exports = class Court {
   constructor(argv) {
	if (! argv) {
	    throw new Error("Must have arguments.");
	}

	if (! argv.precinct) {
	    throw new Error("Must have a precinct.");
	}
	this.precinct = argv.precinct;

	if (! argv.place) {
	    throw new Error("Must have a place.");
	}
	this.place = argv.place;

    };

    toString() {
	return JSON.stringify({ precinct: this.precinct, place: this.place });
    };

    parseExtract(argv) {
	if (! argv) {
	    throw new Error("Must have arguments.");
	}

	if (!argv.database) {
	    throw new Error("Must have database.");
	}

	if (!argv.extractStream) {
	    throw new Error("Must have extractStream.");
	}

	return new Promise((resFN, rejFN) => {
	    let parser = extractParserGenerator(this, resFN, rejFN, argv.database);
	    argv.extractStream
		.on("end", () => { parser.done(); })
		.on("data", d => { parser.parseChunk(d); })
	    ;
	});
    };
    
    getExtract(argv) {
	if (! argv) {
	    throw new Error("Must have arguments.");
	}

	if (! argv.startDate) {
	    throw new Error("Must have startDate.");
	}

	if (! argv.endDate) {
	    throw new Error("Must have endDate.");
	}

	if (! argv.extractType) {
	    throw new Error("Must have extractType.");
	}

	if (!argv.database) {
	    throw new Error("Must have database.");
	}

	return new Promise((resFN, rejFN) => {
	    // Find the court extract ID:
	    let extractCourtID = 0;
	    argv.database.get(
		"SELECT extractID FROM courts WHERE precinct="+this.precinct+" AND place="+this.place,
		(err, row) => {
		    if (err) {
			rejFN({ msg: "court extractID: ", err: err });
		    } else {
			extractCourtID = row.extractID;
			let parser = extractParserGenerator(this, resFN, rejFN, argv.database);
			
			// create the extractURL:
			let extractURL = "https://jpwebsite.harriscountytx.gov/PublicExtracts/GetExtractData?extractCaseType=CV&extract="+argv.extractType+"&court="+extractCourtID+"&casetype=EV&format=xml&fdate="+argv.startDate+"&tdate="+argv.endDate;
						
			// Create request:
			let opts = new httpOpts();
			let req = https.request(extractURL, opts, res => {
			    res
				.on("data", d => { parser.parseChunk(d); })
				.on("end", () => { parser.done(); })
			    ;
			});
			
			req.on('error', e => {
			    console.error(extractURL, e);
			});

			// Ok GO:
			req.end();
		    }
		}
	    );
	});
    };
    
    getDockets(argv) {
	if (! argv) {
	    throw new Error("Must have arguments.");
	}

	return new Promise((resFN, rejFN) => {
	    let startDate = argv.startDate || new Date();

	    let endDate = argv.endDate;
	    if (! argv.endDate) {
		this.endDate = new Date(this.startDate.valueOf());
		this.endDate.setDate(this.startDate.getDate() + 6);
	    }

	    let docketType = argv.docketType || "Eviction Docket";

	    let params = new URLSearchParams({
		"court": (this.precinct * 10 + this.place),
		"fdate": (startDate.getMonth() + 1)+"/"+(startDate.getDate())+"/"+(startDate.getFullYear()),
		"tdate": (endDate.getMonth() + 1)+"/"+(endDate.getDate())+"/"+(endDate.getFullYear())
	    });
	    let opts = new httpOpts();
	    let reqURL = "https://jpwebsite.harriscountytx.gov/WebDockets/GetDocketSummary?"+params.toString();

	    this.dockets = [];
	    
	    // With the data all set up, now connect and parse:
	    let req = https.request(reqURL, opts, res => {
		let handler = new DomHandler((err, dom) => {
		    if (err) {
			rejFN({ url: reqURL, err: err });
		    } else {
			let isEventDescription = (node, type) => {
			    let ret = ("div" === node.name && "record" === node.attribs.class);
			    if (ret && undefined !== type) {
				ret = false;
				// Dig a little deeper. node.children[x].name="span" && node.children[x].attribs.class="eventDescription"
				for (let i = 0; i < node.children.length; i++) {
				    let ci = node.children[i];
				    if ("span" === ci.name && "eventDescription" === ci.attribs.class) {
					// One of the children has to be "text" and match "type":
					for (let j = 0; !ret && j < ci.children.length; j++) {
					    let cj = ci.children[j];
					    ret = ("text" === cj.type && type === cj.data);
					}
				    }
				}
			    }
			    return ret;
			};

			// Find the containing node:
			let events = du.filter(node => {
			    return ("div" === node.name && "contentWithNavBar" === node.attribs.class);
			}, dom)[0];
			
			// Ok. Need to walk the DOM object with the lousy tools I have in javascript.
			let docketPromises = [];
			for (let di = 0; di < events.children.length; di++) {
			    // Looking for: node.name = "div", node.attribs.class = "record"
			    let node = events.children[di];
			    if (isEventDescription(node, docketType)) {
				// Work backwards to find the div.docketTimeHeader:
				let found = false;
				for (let dj = di; !found && dj >= 0; dj--) {
				    let psib = events.children[dj];
				    found = ("div" === psib.name && "docketTimeHeader" === psib.attribs.class);
				    if (found) {
					// Dig out the href:
					for (let si = 0; si < psib.children.length; si++) {
					    let snode = psib.children[si];
					    if ("a" === snode.name) {
						let hURL = new URL(snode.attribs.href, reqURL);
						let docket = new Docket({
						    court: this,
						    URL: hURL,
						    type: docketType
						});
						this.dockets.push(docket);
						docketPromises.push(docket.getCases(argv.caseCallback));
					    }
					}			    
				    }
				}
			    }
			}
			// At this point, not much to return. Let the embedded
			// promises do their work?
			Promise.all(docketPromises)
			    .then(val => {
				resFN({
				    court: this,
				    dockets: val.map(v => {
					if ((v && v instanceof Array && v.length > 0) || v)
					    return v;
				    })
				});
			    })
			    .catch(err => { rejFN(err); })
			;
		    };
		}, {
		    normalizeWhitespace: true
		});
		let parser = new hp.Parser(handler);

		// What to do with incoming data:
		res
		    .on("data", d => { parser.parseChunk(d); })
		    .on("end", () => { parser.done(); })
		;
		
	    });

	    req.on('error', e => {
		console.error(reqURL, e);
	    });

	    // Ok GO:
	    req.end();
	});
    }
};

// Can't do static fields in class definition??
module.exports.EXTRACT_FILED = 1;
module.exports.EXTRACT_HEARINGS = 2;
module.exports.EXTRACT_DISPOSED = 3;
