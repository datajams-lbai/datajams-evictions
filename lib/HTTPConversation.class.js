/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const https = require("https");
const httpOpts = require("./httpOptions");
const tough = require("tough-cookie");

module.exports = class {
    constructor(argv) {
	let opts = argv || {};
	this.debug = opts.debug;
	this.verbose = opts.verbose;
	this.rejFN = opts.rejectFN;

	this.jar = (new tough.CookieJar());
    };
    
    postURLData(argv) {
	let args = argv || {};
	
	if (! args.reqPath) throw "Must have a request path.";
	if (! args.reqHost) throw "Must have a request host.";
	
	let opts = new httpOpts();
	opts.host = args.reqHost;
	opts.path = args.reqPath;
	opts.method = "POST";
	opts.headers["content-type"] = "application/x-www-form-urlencoded";
	delete opts.headers["Accept-Encoding"];
	this.referURL && (opts.headers["referer"] = this.referURL);

	let reqURL = opts.protocol+"//"+opts.host+opts.path;
	opts.headers["cookie"] = this.jar.getSetCookieStringsSync(reqURL);

	this.debug && console.log(opts);
	this.debug && console.log(reqURL);
	
	// With the data all set up, now connect and parse:
	let req = https.request(opts, res => {
	    res.headers["set-cookie"] && res.headers["set-cookie"].map(cookie => { this.jar.setCookieSync(cookie, reqURL); });
	    
	    // What to do with incoming data:
	    let doc = "";
	    res.on("data", d => {
		this.verbose && console.log("postURLData", d.toString());
		if (args.parser) {
		    args.parser.parseChunk(d);
		} else {
		    doc = doc + d;
		}
	    }).on("end", () => {
		this.referURL = reqURL;
		args.parser && args.parser.done();
		args.callback && "function" === typeof args.callback && args.callback(doc);
	    });
	    
	});

	req.on('error', e => {
	    req.abort();
	    if (this.rejFN && "function" === typeof this.rejFN)
		this.rejFN({ fn: "HC.postURLData", url: reqURL, err: e });
	    else
		console.error("HC.postURLData", reqURL, e);
	});

	let us = new URLSearchParams(args.data);
	this.debug && console.log("data: ", us.toString());
	req.write(us.toString());

	// Ok GO:
	req.end();
    };

    getURL(argv) {
	let args = argv || {};
	
	if (! args.reqPath) throw "Must have a request path.";
	if (! args.reqHost) throw "Must have a request host.";
	
	let opts = new httpOpts();
	opts.protocol = "https:";
	opts.host = args.reqHost;
	opts.path = args.reqPath;
	opts.method = "GET";
	this.referURL && (opts.headers["referer"] = this.referURL);

	let reqURL = opts.protocol+"//"+opts.host+opts.path;
	opts.headers["cookie"] = this.jar.getSetCookieStringsSync(reqURL);

	this.debug && console.log(opts);
	this.debug && console.log(reqURL);
	
	// With the data all set up, now connect and parse:
	let req = https.request(opts, res => {
	    res.headers["set-cookie"] && res.headers["set-cookie"].map(cookie => { this.jar.setCookieSync(cookie, reqURL); });

	    // What to do with incoming data:
	    let doc = "";
	    res.on("data", d => {
		this.verbose && console.log("getURL", d.toString());
		if (args.parser) {
		    args.parser.parseChunk(d);
		} else {
		    doc = doc + d;
		}
	    }).on("end", () => {
		this.referURL = reqURL;
		args.parser && args.parser.done();
		args.callback && "function" === typeof args.callback && args.callback(doc);
	    });
	    
	});

	req.on('error', e => {
	    req.abort();
	    if (this.rejFN && "function" === typeof this.rejFN)
		this.rejFN({ fn: "HC.getURL", url: reqURL, err: e });
	    else
		console.error("HC.getURL", reqURL, e);
	});

	// Ok GO:
	req.end();
    };
};
