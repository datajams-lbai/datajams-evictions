/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
// Keep a consistent set of connection options. Can be overridden.
module.exports = class {
    static getGlobalAgent() {
	if (!this.ga) this.gAgent = new (require("https")).Agent({
	    keepAlive: true,
	    maxSockets: 4
	});;
	return this.gAgent;
    };
    
    constructor() {
	this.headers = {
	    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0",
	    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
	    "Accept-Language": "en-US,en;q=0.5",
	    "Accept-Encoding": "identity"
	};
	this.method = "GET";
	this.protocol = "https:";
	this.agent = module.exports.getGlobalAgent();
    };
};
