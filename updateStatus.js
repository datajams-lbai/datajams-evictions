/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const DAO = require("./lib/sqlDAO");
const Case = require("./lib/Case.class.js");
const m=require("dayjs");

let opts = require("./creds")["SQLITE3"];
opts.connectCallback = (database) => {
    // Prep the databse to update each case as we get its updated info:
    let dao = new DAO.CaseCtlr(database);
    dao.stmts.update = dao.prepare(
	database,
	"UPDATE cases SET filed_Date = $fd, case_status = $status WHERE casenumber = $cnum"
    );

    // Track all the update promises, then wait until they're done:
    let promises = [];
    Case.sqlAllCases({
	database: database,
	query: "SELECT case_URL FROM cases WHERE case_status IS NULL OR case_status = 'Active' OR filed_Date IS NULL LIMIT 100",
	// This is the callback used for each case:
	rowCallback: caseObj => {
	    promises.push(
		// Load the data from the website, then 
		caseObj.loadCaseFromURL()
		    .then(caseO => {
			// store it into the database:
			caseO.storeSQL(database);
			(caseO.caseNumber && caseO.filedDate && caseO.caseStatus) &&
			    dao.update({
				$cnum: caseO.caseNumber,
				$fd: (new m(caseO.filedDate).format("YYYY-MM-DD")),
				$status: caseO.caseStatus
			    });
		    })
		    .catch(e => { console.error("loadCaseFromURL", caseObj, e); })
	    );
	},
	completionCallback: () => {
	    console.log("Updating", promises.length, "cases.");
	    // And this is the callback used when the SELECT is finished:
	    Promise.all(promises)
		.finally(() => {
		    // Shut down the database, and the HTTPS agent:
		    require("./lib/httpOptions").getGlobalAgent().destroy();
		    dao.finalize();
		    DAO.disconnect(database);
		})
	    ;
	}
    });
};

DAO.connect(opts);
