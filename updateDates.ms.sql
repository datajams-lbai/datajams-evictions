update cases set filed_date=concat(substring(filed_date, 7, 12), '-', substring(filed_date, 1, 2), '-',substring(filed_date, 4, 2)) where filed_date like '%/2020' ;

update cases set filed_date=concat(right(filed_date, 4),'-01-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'January %' ;
update cases set filed_date=concat(right(filed_date, 4),'-02-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'February %' ;
update cases set filed_date=concat(right(filed_date, 4),'-03-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'March %' ;
update cases set filed_date=concat(right(filed_date, 4),'-04-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'April %' ;
update cases set filed_date=concat(right(filed_date, 4),'-05-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'May %' ;
update cases set filed_date=concat(right(filed_date, 4),'-06-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'June %' ;
update cases set filed_date=concat(right(filed_date, 4),'-07-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'July %' ;
update cases set filed_date=concat(right(filed_date, 4),'-08-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'August %' ;
update cases set filed_date=concat(right(filed_date, 4),'-09-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'September %' ;
update cases set filed_date=concat(right(filed_date, 4),'-10-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'October %' ;
update cases set filed_date=concat(right(filed_date, 4),'-11-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'November %' ;
update cases set filed_date=concat(right(filed_date, 4),'-12-', substring(filed_date, charindex(' ', filed_date)+1, 2),50) where filed_date like 'December %' ;

update events set dateAdded=concat(right(dateAdded, 4),'-01-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'January %' ;
update events set dateAdded=concat(right(dateAdded, 4),'-02-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'February %' ;
update events set dateAdded=concat(right(dateAdded, 4),'-03-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'March %' ;
update events set dateAdded=concat(right(dateAdded, 4),'-04-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'April %' ;
update events set dateAdded=concat(right(dateAdded, 4),'-05-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'May %' ;
update events set dateAdded=concat(right(dateAdded, 4),'-06-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'June %' ;
update events set dateAdded=concat(right(dateAdded, 4),'-07-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'July %' ;
update events set dateAdded=concat(right(dateAdded, 4),'-08-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'August %' ;
update events set dateAdded=concat(right(dateAdded, 4),'-09-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'September %' ;
update events set dateAdded=concat(right(dateAdded, 4),'-10-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'October %' ;
update events set dateAdded=concat(right(dateAdded, 4),'-11-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'November %' ;
update events set dateAdded=concat(right(dateAdded, 4),'-12-', substring(dateAdded, charindex(' ', dateAdded)+1, 2),50) where dateAdded like 'December %' ;
