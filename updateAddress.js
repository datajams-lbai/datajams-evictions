/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const DAO = require("./lib/sqlDAO");
const Case = require("./lib/Case.class");
const OdysseyInfo = require("./lib/OdysseyInfo.class");

let rowLimit = 25;
if (process.argv.length > 2) {
    let n = Number(process.argv[2]);
    if (!Number.isNaN(n) && n.valueOf() > 0) rowLimit = n.valueOf();
}

let opts = require("./creds")["SQLITE3"];
opts.connectCallback = database => {
    // Prep the databse to update each case as we get its updated info:
    let pDao = new DAO.PartyCtlr(database);
    pDao.stmts.update = pDao.prepare(
	database,
	"UPDATE party SET party_address = $pa WHERE casenumber = $cnum AND party_role = $pr AND party_name = $pn"
    );
    pDao.ignoreInsertErrors = true;
    let cDao = new DAO.CaseCtlr(database);
    cDao.stmts.update = cDao.prepare(
	database,
	"UPDATE cases SET odyssey_ID = $oid WHERE casenumber = $cnum"
    );
    

    // Track all the update promises, then wait until they're done:
    let promises = [];
    database.each(
	"SELECT casenumber as caseNumber FROM cases WHERE casenumber IS NOT NULL AND odyssey_ID IS NULL ORDER BY filed_date DESC LIMIT "+rowLimit,
	// This is the callback used for each case:
	(err, caseObj) => {
	    if (! caseObj.caseNumber) console.error("Database had case with no casenuber.");
	    else {
		promises.push(
		    // Grab the 
		    (new OdysseyInfo(caseObj.caseNumber))
			.findCasePromise()
			.then(oi => {
			    // Store the OdysseyID into Cases
			    cDao.update({
				$cnum: oi.caseNumber,
				$oid: oi.caseID
			    });
			    // Store the party addresses
			    oi.parties.forEach(party => {
				if (party.address) {
				    // If this has been a bulk extract,
				    // probably should insert first, given
				    // the bulk extracts only have 2 parties
				    // per case.
				    pDao.insert({
					$addr: party.address.join(" "),
					$cnum: oi.caseNumber,
					$role: party.role,
					$name: party.name
				    });
				    pDao.update({
					$pa: party.address.join(" "),
					$cnum: oi.caseNumber,
					$pr: party.role,
					$pn: party.name
				    });
				}
			    });
			})
			.catch(err => {
			    console.error("Error retrieving OI", caseObj, err);
			})
		);
	    }
	},
	() => {
	    console.log("Updating", promises.length, "addresses.");
	    // And this is the callback used when the SELECT is finished:
	    Promise.all(promises)
		.finally(() => {
		    // Shut down the database, and the HTTPS agent:
		    pDao.finalize();
		    cDao.finalize();
		    DAO.disconnect(database);
		    require("./lib/httpOptions").getGlobalAgent().destroy();
		})
	    ;
	}
    );
};

DAO.connect(opts);
