update cases set filed_date= substr(filed_date, 7, 12)|| '-'|| substr(filed_date, 1, 2)|| '-'||substr(filed_date, 4, 2) where filed_date like '%/2020' ;

update cases set filed_date=substr(filed_date, -4)||'-01-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'January %' ;
update cases set filed_date=substr(filed_date, -4)||'-02-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'February %' ;
update cases set filed_date=substr(filed_date, -4)||'-03-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'March %' ;
update cases set filed_date=substr(filed_date, -4)||'-04-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'April %' ;
update cases set filed_date=substr(filed_date, -4)||'-05-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'May %' ;
update cases set filed_date=substr(filed_date, -4)||'-06-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'June %' ;
update cases set filed_date=substr(filed_date, -4)||'-07-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'July %' ;
update cases set filed_date=substr(filed_date, -4)||'-08-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'August %' ;
update cases set filed_date=substr(filed_date, -4)||'-09-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'September %' ;
update cases set filed_date=substr(filed_date, -4)||'-10-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'October %' ;
update cases set filed_date=substr(filed_date, -4)||'-11-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'November %' ;
update cases set filed_date=substr(filed_date, -4)||'-12-'|| substr(filed_date, charindex(' ', filed_date)+1, 2) where filed_date like 'December %' ;

update events set dateAdded=substr(dateAdded, -4)||'-01-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'January %' ;
update events set dateAdded=substr(dateAdded, -4)||'-02-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'February %' ;
update events set dateAdded=substr(dateAdded, -4)||'-03-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'March %' ;
update events set dateAdded=substr(dateAdded, -4)||'-04-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'April %' ;
update events set dateAdded=substr(dateAdded, -4)||'-05-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'May %' ;
update events set dateAdded=substr(dateAdded, -4)||'-06-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'June %' ;
update events set dateAdded=substr(dateAdded, -4)||'-07-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'July %' ;
update events set dateAdded=substr(dateAdded, -4)||'-08-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'August %' ;
update events set dateAdded=substr(dateAdded, -4)||'-09-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'September %' ;
update events set dateAdded=substr(dateAdded, -4)||'-10-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'October %' ;
update events set dateAdded=substr(dateAdded, -4)||'-11-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'November %' ;
update events set dateAdded=substr(dateAdded, -4)||'-12-'|| substr(dateAdded, charindex(' ', dateAdded)+1, 2) where dateAdded like 'December %' ;
