# datajams-evictions

## Software Needed
* [node.js:](https://nodejs.org). This is known to run in version 10.x. Higher versions may work.
* **npm** : should be included in the download above.

Local data storage is in [SQLITE3](https://sqlite.org). You should only need to download software if you want to independently access the database.

Remote data storage is (currently) in MS SQL Server on Azure. You will need the **mssql** javascript package from npm to connect.

## How to Use
1. Download node.js as above.
1. run `npm install` to pull in the dependencies.
1. copy "creds.example.js" to "creds.js" and edit it to put in your remote database credentials.
1. initialize your local SQLITE3 database
	1. if you have install SQLITE3 as above: `sqlite3 eviction-data.sq3 < schema.sqlite.sql`
	   * otherwise, someone could make a script like `node initSqliteDB`
1. run 
	1. `node loadDockets` to load in docket information to a local SQLITE3 database
	1. `node updateStatus` to update the status of each of the cases in the database
	1. `node updateAddress` to update 10 cases to get party addresses. 
	   * Note: It seems you can do this about once a minute. Any faster and they seem to lock you out.
	1. `node searchNames 'NAME'` to load in cases where NAME is a party
	   * Note: the quotes are really helpful especially if NAME has a space in it, such as '2015 HOUSTON'
1. Transfer data **to Azure**: `node xferData`
    * **NOTE WELL** Don't do this. It will erase all the data up at Azure and replace it with the data from your local SQLITE3 database.
    * You probably don't have as much data as I do.
    * Maybe someone could make a script like `node pullData` to grab data from Azure to local.
