/* Credentials file EXAMPLE.
 * Copy this to "creds.js" so it can be found by scripts.
 * Put in your connection credentials.
 * Don't check it into git!!
 */
module.exports = {
    MS: {
        user: <your MS SQL-Server username>,
        pass: <your MS SQL-Server password>
    },
    // Leave this alone:
    SQLITE3: {
	fileName: "eviction-data.sq3"
    }
};
