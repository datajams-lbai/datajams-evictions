/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const DAO = require("./lib/sqlDAO");
const Case = require("./lib/Case.class.js");
const m=require("dayjs");

if (process.argv.length < 3) throw "Must pass in search string.";
let search = process.argv[2];

if (search.length < 8) throw "Search string must be at least 8 characters.";

let opts = require("./creds")["SQLITE3"];

opts.connectCallback = (database) => {
    let promises = [];
    let dao = new DAO.DAO();
    dao.stmts.update = dao.prepare(
	database,
	"UPDATE cases SET filed_Date = $fd, case_status = $status WHERE casenumber = $cnum"
    );

    Case.findCasesByBusiness(search)
	.then(cases => {
	    cases.forEach((caseUrl) => {
		let caseObj = new Case(null, caseUrl);
		promises.push(
		    caseObj.loadCaseFromURL()
			.then(caseO => {
			    if (caseO.filedDate) {
				caseO.filedDate = (new m(caseO.filedDate).format("YYYY-MM-DD"));
			    }
			    caseO.storeSQL(database);
			    dao.update({
				$cnum: caseO.caseNumber,
				$fd: caseO.filedDate,
				$status: caseO.caseStatus
			    });
			})
			.catch(err => { console.error("Load error", caseObj, err); })
			.finally(() => { console.log("Loaded ", caseObj); })
		);
	    });
	})
	.catch(err => { console.log("searchNames", err); })
	.finally(() => {
	    console.log("Waiting to store "+promises.length+" cases.");
	    Promise.all(promises)
    		.finally(() => {
		    console.log("Shutting down.");
		    // Shut down the database, and the HTTPS agent:
		    require("./lib/httpOptions").getGlobalAgent().destroy();
		    dao.finalize();
		    DAO.disconnect(database);
		})

	    ;
	})
    ;
};

DAO.connect(opts);
