/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const DAO = require("./lib/sqlDAO");
const Court = require("./lib/Court.class");

let today = new Date();
let future = (new Date(today.valueOf()));
future.setDate(today.getDate() + 6);

// Emit the CSV headers; we get a line for each case, below:
console.log(
    '"Case Number",',
    '"Precinct",',
    '"Place",',
    '"Date",',
    '"Time",',
    '"DateTime",',
    '"Claim",',
    '"Plaintiff",',
    '"Defendant 1",',
    '"Defendant 2",',
    '"Defendant 3",',
    '"Case URL",',
    '"Docket URL"'
);

let opts = require("./creds")["SQLITE3"];

opts.connectCallback = (database) => {
    let promises = [];

    // Loop over all 8 precincts, two places per precinct:
    for (let pct = 1; pct <= 8; pct ++) {
	for (let plc = 1; plc <= 2; plc ++) {
	    // param is pct * 10 + plc:
	    let court = new Court({
		precinct: pct,
		place: plc
	    });

	    promises.push(court.getDockets({
		startDate: today,
		endDate: future,
		docketType: "Eviction Docket",
		caseCallback: (caseObj) => {
		    // First, store the case into the database:
		    caseObj.storeSQL(database);

		    // These functions allow the "pivot" below on each case:
		    let plaintiffs = caseObj.parties.filter((party) => {
			return ("Plaintiff" === party.role);
		    });
		    let defendants = caseObj.parties.filter((party) => {
			return ("Defendant" === party.role);
		    });

		    // See the CSV header above? Emit each case to that format:
		    console.log(
			caseObj.caseNumber + ",",
			caseObj.docket.court.precinct + ",",
			caseObj.docket.court.place + ",",
			caseObj.docket.date + ",",
			caseObj.docket.time + ",",
			'"' +
			    caseObj.docket.date.substr(6)+"-"+
			    caseObj.docket.date.substr(0, 2)+"-"+
			    caseObj.docket.date.substr(3, 2)+" "+
			    caseObj.docket.time
			    + '",',
			'"' + caseObj.claimAmount + '",',
			(plaintiffs.length > 0) ? '"' + plaintiffs[0].name + '",' : '"",',
			(defendants.length > 0) ? '"' + defendants[0].name + '",' : '"",',
			(defendants.length > 1) ? '"' + defendants[1].name + '",' : '"",',
			(defendants.length > 2) ? '"' + defendants[2].name + '",' : '"",',
			'"' + caseObj.URL + '",',
			'"' + caseObj.docket.URL.href +'"'
		    );
		}
	    }));
	}
    }

    // Wait for all the dockets to load, then emit their info:
    Promise.all(promises)
	.then(val => { console.error("All courts: ", val); })
	.catch(err => { console.error("All courts ERR: ", err); })
	.finally(() => {
	    console.error("Finished loading all dockets from all courts.");
	    (require("./lib/httpOptions").getGlobalAgent().destroy());
	    DAO.disconnect(database);
	});
    ;
};

DAO.connect(opts);
