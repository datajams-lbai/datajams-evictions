/*
  This file is part of datajams-evictions.

  datajams-evictions is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  datajams-evictions is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with datajams-evictions.  If not, see <https://www.gnu.org/licenses/>.

  Copyright 2020 Luigi Bai
*/
const Court = require("./lib/Court.class");
const sqDAO = require("./lib/sqlDAO");
const moment = require("dayjs");

if (process.argv.length < 4) throw "Must pass in YYYY and MM.";
let year = process.argv[2];
let month = process.argv[3];

let sDate = moment(year+"-"+month+"-01", "YYYY-MM-DD");
if (! sDate.isValid()) throw "Invalid month requested.";
let eDate = moment(sDate).add(1, "month").subtract(1, "day");

let opts = require("./creds")["SQLITE3"];
opts.connectCallback = sqDB => {
    let exPromises = [];
    
    for (var pct of [1, 2, 3, 4, 5, 6, 7, 8]){
	for (var plc of [1, 2]) {
	    let court = new Court({
		precinct: pct,
		place: plc
	    });
	    for (var et of [Court.EXTRACT_FILED, Court.EXTRACT_DISPOSED]) {
		let exOpts = {
		    database: sqDB,
		    startDate: sDate.format("MM/DD/YYYY"),
		    endDate: eDate.format("MM/DD/YYYY"),
		    extractType: et
		};
		exPromises.push(
		    court.getExtract(exOpts)
			.then(vals => {
			    console.log(exOpts);
			    vals.forEach(val => {
				console.log("Case", val, "stored");
			    });
			})
			.catch(err => {
			    console.error("ERROR", err);
			})
		);
	    }
	}
    }
    Promise.all(exPromises)
	.finally(() => {
	    sqDAO.disconnect(sqDB);
	})
    ;
};
sqDAO.connect(opts);
